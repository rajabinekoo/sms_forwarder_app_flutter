import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:background_sms/background_sms.dart' as BgSmsSender;
import 'package:permission_handler/permission_handler.dart';
import 'package:telephony/telephony.dart';
import 'package:path_provider/path_provider.dart';

var targetMobileStorageKey = "target-mobile-key";
var sourceMobileStorageKey = "source-mobile-key";

SourceNumberStorage sourceNumberStorage = SourceNumberStorage();
DestinationNumberStorage destinationNumberStorage = DestinationNumberStorage();

void getPermission() async => await [
      Permission.sms,
    ].request();

Future<bool> isPermissionGranted() async =>
    await Permission.sms.status.isGranted;

void sendMessage(String phoneNumber, String message, {int? simSlot}) async {
  var result = await BgSmsSender.BackgroundSms.sendMessage(
      phoneNumber: phoneNumber, message: message, simSlot: simSlot);
  if (result == BgSmsSender.SmsStatus.sent) {
    // print("Sent");
  } else {
    // print("Failed");
  }
}

Future<bool?> get supportCustomSim async =>
    await BgSmsSender.BackgroundSms.isSupportCustomSim;

void backgrounMessageHandler(SmsMessage message) async {
  // print(message.address); //+977981******67, sender nubmer
  // print(message.body); //sms text
  // print(message.date); //1659690242000, timestamp

  String targetMobileValue = await destinationNumberStorage.readValue();
  String sourceMobileValue = await sourceNumberStorage.readValue();

  // print("sourceMobileValue");
  // print(sourceMobileValue.toString());
  // print(message.address.toString());
  // print(sourceMobileValue.toString().toLowerCase() ==
  // message.address.toString().toLowerCase();
  if (sourceMobileValue.toString().toLowerCase() ==
      message.address.toString().toLowerCase()) {
    if (await isPermissionGranted()) {
      if ((await supportCustomSim)!) {
        sendMessage(targetMobileValue, message.body.toString(), simSlot: 1);
      } else {
        sendMessage(targetMobileValue, message.body.toString());
      }
    } else {
      await [
        Permission.sms,
      ].request();
      if (await isPermissionGranted()) {
        if ((await supportCustomSim)!) {
          sendMessage(targetMobileValue, message.body.toString(), simSlot: 1);
        } else {
          sendMessage(targetMobileValue, message.body.toString());
        }
      }
    }
  }
}

void main() {
  runApp(const MainLayout());
}

class MainLayout extends StatelessWidget {
  const MainLayout({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SMS Forwarder',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(icon: Text("Main")),
                Tab(icon: Text("Settings")),
              ],
            ),
            title: const Text('SMS Forwarder'),
          ),
          body: const TabBarView(
            children: [
              MainTab(),
              SetPhoneNumbersTab(),
            ],
          ),
        ),
      ),
    );
  }
}

// ==================================================
// TABS
// ==================================================

class MainTab extends StatelessWidget {
  const MainTab({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const EnableOrDisableApp(),
    );
  }
}

// ==================================================
// CONTAINERS
// ==================================================

class SetPhoneNumbersTab extends StatelessWidget {
  const SetPhoneNumbersTab({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const Scaffold(
          body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Card(
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: PhoneNumbersForm(),
                ),
              ))
        ],
      )),
    );
  }
}

class EnableOrDisableApp extends StatefulWidget {
  const EnableOrDisableApp({super.key});

  @override
  State<EnableOrDisableApp> createState() => _EnableOrDisableApp();
}

class _EnableOrDisableApp extends State<EnableOrDisableApp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Telephony telephony = Telephony.instance;
  TextEditingController sourceNumberController = TextEditingController();

  void checkBgSmsSenderPermission() async {
    if (!(await isPermissionGranted())) {
      await [
        Permission.sms,
      ].request();
    }
    bool? permissionsGranted = await telephony.requestPhoneAndSmsPermissions;

    bool success = await isPermissionGranted() && permissionsGranted!;

    final snackBar = SnackBar(
      content:
          success ? const Text('Enabled.') : const Text('Permission problem!'),
    );

    // ignore: use_build_context_synchronously
    ScaffoldMessenger.of(context).showSnackBar(snackBar);

    if (success) {
      telephony.listenIncomingSms(
          onNewMessage: (SmsMessage message) {
            backgrounMessageHandler(message);
          },
          listenInBackground: true,
          onBackgroundMessage: backgrounMessageHandler);
    }
  }

  void closeApp() {
    exit(0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: OutlinedButton(
                onPressed: checkBgSmsSenderPermission,
                child: const Text("Enable forwarder"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: OutlinedButton(
                onPressed: closeApp,
                child: const Text("Disable forwarder"),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class PhoneNumbersForm extends StatefulWidget {
  const PhoneNumbersForm({super.key});

  @override
  State<PhoneNumbersForm> createState() => _PhoneNumbersForm();
}

class _PhoneNumbersForm extends State<PhoneNumbersForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController sourceNumberController = TextEditingController();
  TextEditingController targetNumberController = TextEditingController();

  String _targetNumber = "";
  String _sourceNumber = "";

  void resolvePhoneNumbers() async {
    String targetMobileValue = await destinationNumberStorage.readValue();
    String sourceMobileValue = await sourceNumberStorage.readValue();
    setState(() {
      _targetNumber = targetMobileValue.toString();
      _sourceNumber = sourceMobileValue.toString();
    });
  }

  @override
  void initState() {
    resolvePhoneNumbers();
    super.initState();
  }

  void onSubmit() async {
    if (_formKey.currentState!.validate()) {
      sourceNumberStorage.writeValue(sourceNumberController.text.toString());
      destinationNumberStorage
          .writeValue(targetNumberController.text.toString());
      setState(() {
        setState(() {
          _targetNumber = targetNumberController.text.toString();
          _sourceNumber = sourceNumberController.text.toString();
        });
      });
    }
  }

  void onClear() async {
    sourceNumberStorage.writeValue('');
    destinationNumberStorage.writeValue('');
    setState(() {
      setState(() {
        _targetNumber = '';
        _sourceNumber = '';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: const InputDecoration(
              hintText: 'Enter source phone number',
            ),
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
            controller: sourceNumberController,
          ),
          TextFormField(
            decoration: const InputDecoration(
              hintText: 'Enter destination phone number',
            ),
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
            controller: targetNumberController,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Column(children: <Widget>[
              Text(
                'Source number: $_sourceNumber',
                style: Theme.of(context).textTheme.titleSmall,
                textAlign: TextAlign.left,
              ),
              Text(
                'Destination number: $_targetNumber',
                style: Theme.of(context).textTheme.titleSmall,
                textAlign: TextAlign.left,
              ),
            ]),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: OutlinedButton(
                  onPressed: onSubmit,
                  child: const Text('Submit'),
                ),
              ),
              const SizedBox(width: 50),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: onClear,
                  child: const Text('Clear'),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

// storages

class SourceNumberStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/sourceNumber.txt');
  }

  Future<String> readValue() async {
    try {
      final file = await _localFile;

      // Read the file
      final contents = await file.readAsString();

      return contents.toString();
    } catch (e) {
      // If encountering an error, return 0
      return '';
    }
  }

  Future<File> writeValue(String number) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString('$number');
  }
}

class DestinationNumberStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/destinationNumber.txt');
  }

  Future<String> readValue() async {
    try {
      final file = await _localFile;

      // Read the file
      final contents = await file.readAsString();

      return contents.toString();
    } catch (e) {
      // If encountering an error, return 0
      return '';
    }
  }

  Future<File> writeValue(String number) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString('$number');
  }
}
